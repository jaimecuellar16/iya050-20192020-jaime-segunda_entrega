const { GraphQLServer } = require('graphql-yoga')
const fetch = require('node-fetch')

const SERVICE_URL = "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs/"
let index=0;


const resolvers = {
  Query: {

    //Get Specific question
    getMeme: (_, {id}) => fetch(`${SERVICE_URL}{id}`, {
      method: 'get',
      headers: { 'Content-Type': 'application/json', 'x-application-id':'jaime.cuellar' },
    }).then(res=>{
      console.log(res);
      return res.json();
    }),
    getAllMemes: (_, {}) => fetch(`${SERVICE_URL}`, {
      method: 'get',
      headers: { 'Content-Type': 'application/json', 'x-application-id':'jaime.cuellar' },
    }).then(res=>{
      console.log(res);
      return res.json();
    })




  },
  Mutation: {
    
    createMeme: (_, {
      id,
      url,
      text
    }) => {
      console.log(id);
      let id_ = index++;
      fetch(`${SERVICE_URL}meme${id_}`, {
        method:"put",
        headers:{
        "Content-Type": "application/json", 'x-application-id':'jaime.cuellar'
        },
        body: JSON.stringify({
          url:url,
          text:text
        })
        }).then(res=>{
        console.log(res);
        return res.json();
      })
    }
  }
}

  
const server = new GraphQLServer({ typeDefs:"./memeSchema.graphql", resolvers })


  server.start(
    {
      cors: {
        origin: "*",
        headers: ["*"],
        allowed_methods: ["HEAD", "GET", "POST", "PUT"],
        allowed_headers: ["*"]
      }
    },
    server => {
      console.log(`Server is running on http://localhost/${server.port}`);
    }
  );
