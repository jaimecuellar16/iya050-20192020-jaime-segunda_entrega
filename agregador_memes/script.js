const { React, ReactDOM, HomeView, UploadView, MemesView } = window;
const { BrowserRouter: Router, Link, Switch, Route } = window.ReactRouterDOM;

class Script extends React.Component {
  constructor(props) {
    super(props);
  }
 
  render() {
    return (
      <Router>
        <nav>
          <ul>
            <li>
              <Link to="/home">Go to Home</Link>
            </li>
            <li>
              <Link to="/Upload">Go to Upload</Link>
            </li>
            <li>
              <Link to="/Memes">Go to Meme</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/home">
            <HomeView />
          </Route>
          <Route path="/Upload">
            <UploadView />
          </Route>
          <Route path="/Memes">
            <MemesView />
          </Route>
        </Switch>
      </Router>
      /* <HomeView />
      <MemesView />
        <UploadView />*/
    );
  }
}

ReactDOM.render(<Script />, document.getElementById("root"));