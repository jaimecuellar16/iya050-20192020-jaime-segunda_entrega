# Agregador de memes

## Requisitos de la primera entrega

- Utilizar NPM scripts y un index.html con tags `<script>`, no `create-react-app`. Podéis ver ejemplos en los archivos de Glitch que hemos hecho en clase hasta ahora.
- Utilizar ESlint y Prettier. La aplicación debe ser una SPA.
- Vista con lista de memes. Debe usar un stub en lugar de datos obtenidos de un servidor remoto.
- Vista con un meme concreto. Debe usar un stub en lugar de datos obtenidos de un servidor remoto.
- Funcionalidad de `share`. Debe marcar visualmente los memes como compartidos al ser usada.
- Formulario de creación de meme. Debe hacer una petición a un servidor dado aún por confirmar.

##Requisitos de la segunda entrega

Esta entrega consiste en implementar un servidor utilizando GraphQL e integrarlo con una aplicación web.

La funcionalidad de la aplicación resultante debe ser la misma que la descrita en el enunciado de la primera entrega. Sin embargo, en esta entrega todos los datos de la aplicación deben ser manejados por el servidor. Esto implica que:

-El proyecto no contendrá stubs con datos estáticos.
-El servidor almacenará de forma persistente los datos introducidos en los formularios.
-La aplicación mostrará los datos persistidos de esa manera en sus diferentes componentes.
-Para evitaros la complejidad de manejar una base de datos, debéis utilizar Volatile como servicio de -almacenamiento. Tened en cuenta que, por los límites que tiene Volatile no podréis almacenar imágenes.

La fecha límite para entregar el trabajo es el 15 de abril, inclusive.

### Despliegue

https://memes-front.glitch.me/