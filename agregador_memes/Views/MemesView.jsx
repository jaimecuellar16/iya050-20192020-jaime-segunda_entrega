const { React, ReactDOM, Meme, Comments, Navbar, Sidebar } = window;

class MemesView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Sidebar />
        <Meme />
        <Comments />
        <Navbar />
      </div>
    );
  }
}
