const { React, ReactDOM, FormMeme, Navbar, Sidebar } = window;

class UploadView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Sidebar />
        <FormMeme />
        <Navbar />
      </div>
    );
  }
}
