const { React } = window;

class Comments extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="comments">
        <div className="row">
          <div className="col-25">
            <label>User</label>
          </div>
          <div className="col-75">
            <textarea
              id="subject"
              name="subject"
              placeholder="Write something Funny...... please"
            />
          </div>
        </div>
      </div>
    );
  }
}
