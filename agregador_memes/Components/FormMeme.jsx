const { React } = window;

class FormMeme extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      id:" ",
      url:" ",
      text:" "
    };

    this.uploadMeme = this.uploadMeme.bind(this);
    this.setUrl = this.setUrl.bind(this);
    this.setText = this.setText.bind(this);
    this.setID = this.setID.bind(this);

  }

  setUrl(event){
    this.setState({url:event.target.value});
  }

  setText(event){
    this.setState({text:event.target.value});
  }
  setID(event){
    this.setState({id:event.target.value});
  }

  uploadMeme(){
    console.log(this.state.url);
    console.log(this.state.text);
    fetch("https://meme-graphql.glitch.me",{
      method:"POST",
      headers:{
        "Content-Type":"application/json",

      },
      body: JSON.stringify({
        query:`mutation{
          createMeme(
            id:"`+this.state.id+`",
            url:"`+this.state.url+`",
            text:"`+this.state.text+`"
          ){
            value
            key
            applicationId
          }
        }`
      })
    }).then(function(response){
      response.json().then(function(data){
        console.log(data);
        alert("Se envio la data");
      })
    })
  }
  render() {
    return (
      <div className="comments">
          <div className="row">
            <div className="col-25">
              <label>Awesome Meme</label>
            </div>
            <div className="col-75">
              <input
                type="text"
                id="lname"
                placeholder="Name your meme"
                onChange={this.setID}
              />
            </div>
            <div className="col-75">
              <input
                type="text"
                id="lname"
                placeholder="Image url"
                onChange={this.setUrl}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-25">
              <label>Description</label>
            </div>
            <div className="col-75">
              <textarea
                id="subject"
                placeholder="Write something an awesome meme Description.."
                onChange={this.setText}
              />
            </div>
          </div>
          <center>
            <button id="memeButton" onClick={this.uploadMeme}>Upload meme</button>
          </center>
      </div>
    );
  }
}
