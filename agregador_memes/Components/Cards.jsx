const { React , ReactDOM } = window;


class Cards extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      memes:[{url:"https://www.quirkybyte.com/wp-content/uploads/2018/03/Captain-America-1.jpg",text:"JAJA"}]
    };
    this.getMemes = this.getMemes.bind(this);
  }

  getMemes(){
    let component = this;
    const regex = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i;
    console.log(component);
    fetch("https://meme-graphql.glitch.me",{
      method:"POST",
      headers:{
        "Content-Type":"application/json"
      },
      body: JSON.stringify({
        query:`
        query{
          getAllMemes{
            createdAt
            value
            key
            applicationId
          }
        }
        `
      })
    }).then(function(res){
      res.json().then(function(data){
        const data_ = data.data.getAllMemes;
        const length = data_.length;
        const memes = component.state.memes;
        for (var i=0;i<length-1;i++){
          let json_meme = JSON.parse(data_[i].value);
          if(regex.test(json_meme.url)) memes.push(json_meme)
        }
        console.log(memes);
        component.setState({memes:memes});
      })
    })
  }
  componentDidMount(){
    this.getMemes();
  }
  changeColor1() {
    document.getElementById("shareButton1").style.backgroundColor = "purple"; // backcolor
    document.getElementById("shareButton1").innerHTML = "SHARED";
  }

  changeColor2() {
    document.getElementById("shareButton2").style.backgroundColor = "purple"; // backcolor
    document.getElementById("shareButton2").innerHTML = "SHARED";
  }

  changeColor3() {
    document.getElementById("shareButton3").style.backgroundColor = "purple"; // backcolor
    document.getElementById("shareButton3").innerHTML = "SHARED";
  }
  render() {
    console.log(this.state.memes);
      return this.state.memes.map((elem)=>(
        <div className="card">
          <img
            src={elem.url}
            alt="Avatar"
          />
          <div className="container">
            <p id="p1">{elem.text}</p>
            <h4>
              <button id="memeButton">+UP</button>
              <button id="memeButton">-DOWN</button>
              <button id="memeButton">COMMENT</button>
              <button id="shareButton1" onClick={this.changeColor1}>
                SHARE
              </button>
            </h4>
          </div>
        </div>
      ));
  }
}
