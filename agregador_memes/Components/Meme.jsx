const { React } = window;

class Meme extends React.Component {
  constructor(props) {
    super(props);
  }

  changeColor() {
    document.getElementById("shareButton1").style.backgroundColor = "purple"; // backcolor
    document.getElementById("shareButton1").innerHTML = "SHARED";
  }
  render() {
    return (
      <div className="meme">
        <img
          src="https://www.quirkybyte.com/wp-content/uploads/2018/03/Captain-America-1.jpg"
          alt="Avatar"
        />
        <div className="container">
          <p>The misterious thing called life</p>
          <h4>
            <button id="memeButton">+UP</button>
            <button id="memeButton">-DOWN</button>
            <button id="memeButton">COMMENT</button>
            <button id="shareButton1" onClick={this.changeColor}>
              SHARE
            </button>
          </h4>
        </div>
      </div>
    );
  }
}
